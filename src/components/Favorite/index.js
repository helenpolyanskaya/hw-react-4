import React from 'react';
import starunfilled from '../../image/starunfilled.png';
import './favorite.css';
import { useDispatch, useSelector } from "react-redux";
// import PropTypes from 'prop-types';

export default function Favorite() {

    const favoriteCards = useSelector((state) => state.favoriteReducer.cards)

   return(
        <div className='favorite_wrapper'>
            <a>
                <img src={starunfilled}></img>
            </a>
            <div className='favorite-count'>
                {favoriteCards.length}
            </div>
        </div>
    )
}


// Favorite.propTypes = {
//     name: PropTypes.string,
//     article: PropTypes.number,
//     price: PropTypes.number,
//     url: PropTypes.string,
//     color: PropTypes.string
//   };

// class Favorite extends React.Component {

//     constructor(props){
//         super(props);   
//     }

//     render() {

//         return(
//             <div className='favorite_wrapper'>
//                 <a>
//                     <img src={starunfilled}></img>
//                 </a>
//                 <div className='favorite-count'>
//                 {this.props.favoriteCards.length}
//                 </div>
//             </div>
//         )
//     }
// }  

// export default Favorite;