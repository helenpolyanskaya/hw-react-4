import React, { useState, useEffect} from "react";
import Home from "../../pages/Home.js";
import Basketpage from "../../pages/Basketpage.js";
import Favoritepage from "../../pages/Favoritepage.js";
import { NavLink, Routes, Route } from "react-router-dom";
import Modal from '../Modal';
import Cards from '../Cards';
import Basket from '../Basket';
import iconstore from '../../image/iconstore80.png';
import Favorite from '../Favorite';
import { useFetch } from "../../hooks/use_fetch";
import {addToBasket, deleteToBasket} from '../../redux/actions/basket';
import { loadFavorites } from '../../redux/actions/favorite';
import { loadBasket } from '../../redux/actions/basket';
import { useDispatch, useSelector } from "react-redux";
import { fetchCardsAsync } from "../../redux/actions/cards.js";

import './app.css';

export default function App() {
    const dispatch = useDispatch();

    const [openModal, setOpenModal] = useState (false);
    const [modal, setModal] = useState ({});
    // const [cards, setCards] = useState ([]);
    // const { data } = useFetch("card.json");
    // const [basket, setBasket] = useState ([]);
    // const [favoriteCards, setFavorite] = useState ([]);

    const closeModal = () => {
        setOpenModal(false)
    }

    // const addToFavorites = (card) => {
    //     dispatch(addToFavorite(card))
    //     let newFavoriteCards = [...favoriteCards];
    //     if (favoriteCards.map(c => c.article).includes(card.article)) {
    //         newFavoriteCards = favoriteCards.filter(c => c.article != card.article )
    //     } else {
    //         newFavoriteCards.push(card);
    //     }
    //     setFavorite(newFavoriteCards);
    //     localStorage.setItem('Favorite Toys', JSON.stringify(newFavoriteCards));
    // }

    // const deleteFavorites = (card ) => {
    //     console.log(card)
    //     let newFavoriteCards = [...favoriteCards];
    //     console.log(newFavoriteCards);
    //     if (favoriteCards.map(c => c.article).includes(card.article)) {
    //         let idx = newFavoriteCards.findIndex(c => c.article == card.article);
    //         newFavoriteCards.splice(idx, 1)
    //     }
    //     setFavorite(newFavoriteCards);
    //     localStorage.setItem('Favorite Toys', JSON.stringify(newFavoriteCards));
    // }

    // const addToBasket = (card) => {
    //     let newBasket = [...basket];
    //     newBasket.push(card);
    //     setBasket(newBasket);
    //     localStorage.setItem('Toys basket', JSON.stringify(newBasket));
    // }    

    const deleteBasket = (card) => {
        dispatch(deleteToBasket(card))
        // let newBasket = [...basket];
        // if (basket.map(c => c.article).includes(card.article)) {
        //     let idx = newBasket.findIndex(c => c.article == card.article);
        //     newBasket.splice(idx, 1)
        // }
        // setBasket(newBasket);
        // localStorage.setItem('Toys basket', JSON.stringify(newBasket));

    }  
    const clickOpenModal = (modalId, card) => {
        // let modal = modalsData[modalId];
        // modal.header = "In the basket: " + card.name;
        // modal.onAction1 = () => { 
        //     closeModal();
        //     dispatch(addToBasket(card))
        // };

        // setModal(modal);
        // setOpenModal(true);
    }

    const clickEditModal = (modalId, card) => {
        // let modal = modalsData[modalId];
        // modal.header = "Do you want remove " + card.name +"?";
        // modal.onAction1 = () => { 
        //     closeModal();
        //     deleteBasket(card);
        // };

        // setModal(modal);
        // setOpenModal(true);
    }

    
        const cards = useSelector((state) => state.cardsReducer.cards);

    useEffect (() => {            
        // setCards(data);
        // let basketArrayRaw = localStorage.getItem (['Toys basket']) ;
        // let basketArray  = basketArrayRaw ? JSON.parse(basketArrayRaw) : [];
        // setBasket( basketArray );
        dispatch(fetchCardsAsync());

        dispatch(loadBasket());

        dispatch(loadFavorites());

        // let favoriteArrayRaw = localStorage.getItem (['Favorite Toys']) ;
        // let favoriteCards  = favoriteArrayRaw ? JSON.parse(favoriteArrayRaw) : [];
        // setFavorite( favoriteCards );
    }, [])


    return (
        <>  
            <div className='nav_wrapper'>
                <NavLink to="/">
                    <img src={iconstore} alt="Toys"></img>
                </NavLink>    
                <div className='counts_wrapper'>
                    <NavLink to="/favorite">
                        <Favorite/>
                    </NavLink>
                    <NavLink to="/basket">
                        <Basket/>
                    </NavLink>    
                </div>  
            </div>      
            <Routes>
                <Route path="/" element={
                                <div>  
                                <Cards
                                cards = {cards}
                                context = {'Add to basket'}
                                clickOpenModal = {clickOpenModal}
                                // addToFavorites = {addToFavorites}
                                modalId = 'addCard'
                                /> 
                            </div>
                }/>
                <Route path="/basket" element={<Basketpage
                    clickOpenModal = {clickEditModal}
                    // addToFavorites = {addToFavorites}
                    modalId = 'editBasket'
                    page="basket"
                />}/>
                <Route path="/favorite" element={<Favoritepage
                    context = {'Add to basket'}
                    clickOpenModal = {clickOpenModal}
                    // addToFavorites = {deleteFavorites}
                    modalId = 'addCard'
                    page="basket"
                />}/>
            </Routes>
            <Modal 
                // modalsData = {modal}
                // header = {modal.header}
                // text = {modal.text}
                // open = {openModal} 
                // onClose = {closeModal} 
                // actions1 = {modal.actions1} 
                // actions2 = {modal.actions2}
             />

        </>
    )
}
