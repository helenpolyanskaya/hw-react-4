import React from 'react';
import Cards from "../components/Cards";
import { useSelector } from "react-redux";

export default function Favoritepage ({context}) {
    const cards = useSelector((state) => state.favoriteReducer.cards);

    return (

        cards.length>0 ?  <Cards cards = {cards} 
        context= {context}
        /> :  <h1> "Немає обраних товарів!"</h1>
       
    ) 
       
    
}