import { combineReducers } from "redux";

import { cardsReducer } from "./reducers/cards";
import { basketReducer } from "./reducers/basket";
import { favoriteReducer } from "./reducers/favorite";
import { modalReducer } from "./reducers/modal";

export const rootReducer = combineReducers({
    cardsReducer,
    basketReducer,
    favoriteReducer,
    modalReducer,
});