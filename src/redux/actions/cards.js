import { cardsTypes } from "../types";

export function fetchCards(cards) {
  return {
    type: cardsTypes.FETCH_CARDS,
    payload: {
      cards,
    },
  };
}

export function fetchCardsAsync() {
  return async function (dispatch) {
    const data = await fetch(`card.json`).then(
      (res) => res.json()
    );
    dispatch(fetchCards(data));
  };
}
