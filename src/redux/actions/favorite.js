import { favoriteTypes } from "../types";
import { useSelector } from "react-redux";

export function addToFavorite (card) {
     return async function (dispatch) {
          dispatch({
               type: favoriteTypes.FAVORITE_ADD_CARD,
                payload: card
          })
     }
}

export function loadFavorites(){
     return async function (dispatch) {
          const favoriteArrayRaw = localStorage.getItem (['Favorite Toys']) ;
          const favoriteCards  = favoriteArrayRaw ? JSON.parse(favoriteArrayRaw) : [];
          dispatch({
               type: favoriteTypes.LOAD_FAVORITES,
               payload: favoriteCards,
          })
     }
}

// export function deleteFavorite (card) {
//      return async function (dispatch) {
//           dispatch({
//                type: favoriteTypes.FAVORITE_DELETE_CARD,
//                payload: card
//           })
//      }
// }