import { modalTypes } from "../types";

export function closeModalAction() {
     return async function (dispatch) {
          dispatch({
               type: modalTypes.CLOSE_MODAL,
          })
     }
}

export function openAddToBasketModal(card) {
    return {
            type: modalTypes.OPEN_ADD_BASKET_MODAL,
            payload: card,

    }
}

export function openDeleteBasketModal(card) {
    return {
            type: modalTypes.OPEN_DELETE_BASKET_MODAL,
            payload: card
    }
}
