import { basketTypes } from "../types";

const initialState = {
  cards: [],
};

export function basketReducer(state = initialState, action) {
  let newBasket;
  let card;
  switch (action.type) {
    case basketTypes.BASKET_ADD_CARD:
      card = action.payload;
      newBasket = [...state.cards];
      newBasket.push(card);
      localStorage.setItem('Toys basket', JSON.stringify(newBasket));
        return {
          ...state, cards: newBasket
        }; 
    case basketTypes.LOAD_BASKET:
        return {
          ...state, cards: action.payload
        };    
    case basketTypes.BASKET_DELETE_CARD:
      newBasket = [...state.cards];
      card = action.payload;
      if (newBasket.map(c => c.article).includes(card.article)) {
        let idx = newBasket.findIndex(c => c.article == card.article);
        newBasket.splice(idx, 1)
      }
      localStorage.setItem('Toys basket', JSON.stringify(newBasket)); 
      return {
        ...state, cards: newBasket
      } 
        
    default:
        return state;
  }
}