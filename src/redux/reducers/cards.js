import { cardsTypes } from "../types";

const initialState = {
  cards: [],
};

export function cardsReducer(state = initialState, action) {
  switch (action.type) {
    case cardsTypes.FETCH_CARDS:
        return {
            ...action.payload,
      };  
    default:
        return state;
  }
}
