import { modalTypes } from "../types";

const initialState = {
  open: false,
  modalId: 'editBasket',
  card: null,
};

export function modalReducer(state = initialState, action) {
  switch (action.type) {
    case modalTypes.CLOSE_MODAL:
        return {
            ...state, open: false
        };  

    case modalTypes.OPEN_ADD_BASKET_MODAL:
        return {
            ...state, open: true, modalId: 'addCard', card: action.payload
        }

    case modalTypes.OPEN_DELETE_BASKET_MODAL:
        return {
            ...state, open:true, modalId: 'editBasket', card: action.payload
        }   
         
    default:
        return state;
  }
}
