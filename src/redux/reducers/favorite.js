import { favoriteTypes } from "../types";

const initialState = {
  cards: [],
};

export function favoriteReducer(state = initialState, action) {
  switch (action.type) {
    case favoriteTypes.FAVORITE_ADD_CARD:
      const card = action.payload;
      let newFavoriteCards = [...state.cards];
      if (newFavoriteCards.map(c => c.article).includes(card.article)) {
        newFavoriteCards = newFavoriteCards.filter(c => c.article != card.article )
      } else {
          newFavoriteCards.push(card);
      }
      localStorage.setItem('Favorite Toys', JSON.stringify(newFavoriteCards));
        return {
          ...state, cards: newFavoriteCards
        }; 
    case favoriteTypes.LOAD_FAVORITES:
      return {
        ...state, cards: action.payload
      };  
        
    default:
        return state;
  }
}